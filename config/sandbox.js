import credentials from './credentials.js';

const sandbox = {
	"PORT":8031,
	"REDIS_HOST":"http://sandbox.example.com",
	"REDIS_PORT":"6379",
	"REDIS_PASSWORD":credentials.REDIS_PASSWORD,
	"AES256_ENCRYPTION":credentials.AES256_ENCRYPTION,
	"REDIS_DB_INDEX": 0
}

export default sandbox