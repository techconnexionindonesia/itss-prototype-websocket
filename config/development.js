import credentials from './credentials.js';

const development = {
	"PORT":8031,
	"REDIS_HOST":"127.0.0.1",
	"REDIS_PORT":"6379",
	"REDIS_PASSWORD":credentials.REDIS_PASSWORD,
	"AES256_ENCRYPTION":credentials.AES256_ENCRYPTION,
	"REDIS_DB_INDEX": 0
}

export default development