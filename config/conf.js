import development from "./development.js";
import sandbox from "./sandbox.js";
import production from "./production.js";

var env = "development"; // development,sandbox,production
var conf = null;

if (env == "development"){
	conf = development;
} else if (env == "sandbox"){
	conf = sandbox;
} else {
	conf = production;
}

export default conf;