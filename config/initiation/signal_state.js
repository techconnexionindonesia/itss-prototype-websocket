const signal_state = {
	"signal_state:a_j22":"red",
	"signal_state:a_j42":"red",
	"signal_state:a_j10":"red",
	"signal_state:a_mj10":"yellow",
	"signal_state:b_mj24":"yellow",
	"signal_state:b_j24":"red",
	"signal_state:b_j52":"red",
	"signal_state:b_j32":"red",
	"signal_state:b_j12":"red"
}

export default signal_state