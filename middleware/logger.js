import fs from 'fs'
import util from 'util'
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import dateFormat from 'dateFormat';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
var filename=dateFormat(new Date(), "yyyy-mm-dd");

var log_file = fs.createWriteStream(__dirname + '/../logs/'+filename+'.log', {flags : 'w'});
var log_stdout = process.stdout;

var logger = function(filename,log) {
  var datetime = new Date().toISOString().replace(/T/, ' ');
  var text = datetime+" - "+filename+" - "+log;
  log_file.write(util.format(text) + '\n');
  log_stdout.write(util.format(text) + '\n');
};

export default logger;