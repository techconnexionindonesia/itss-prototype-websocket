import validateData from './validator.js';
import Signal from './signal.js';

var self;
var signal;

class Core{
	constructor(parent){
		self = parent;
		signal = new Signal(self);
		this.getAction = this.getAction.bind(this);
		this.prepareResult = this.prepareResult.bind(this);
	}

	initializeEverything(){
		self.logger("core.js","initialize all");
		signal.initializeSignal();
	}

	getAction(data,callback){
		if (typeof data.action != 'undefined'){
			self.logger("core.js","action is : "+data.action);
			switch (data.action){
				case 'change_signal_state':
					signal.updateSignalState(data,function(err,data){
						callback(null,data);
					});
					break;

				default:
					self.logger("core.js","No action available");
					break;
			}
		} else {
			self.logger("core.js","data.action not defined");
		}
	}

	prepareResult(data){
		self.logger("core.js","Prepare Result, encrypting data");
		var result = self.encryption.encrypt(data);
		return result;
	}

	processData(web_socket_connection,data){
		self.core = this;
		self.logger("core.js","Start processing data");
		self.logger("core.js","Data input : "+data);
		if (!validateData(data,self)){
			self.logger("core.js","Data incorrect. Request aborted");
			return;
		} else {
			// Temporary
			var encrypted = self.encryption.encrypt(data);
			
			var decrypted = self.encryption.decrypt(encrypted);
			decrypted = JSON.parse(decrypted);
			self.logger("validator.js","Decrypted : "+decrypted);
			this.getAction(decrypted,function(err,data){
				if (!err){
					var result = self.core.prepareResult(data);
					self.web_socket_server.broadcast(result,self.ws);
				}
			});
		}
	}
}

export default Core