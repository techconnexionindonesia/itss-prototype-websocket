var parent;

const validateData = function(data,parent_instance){
	parent = parent_instance;
	parent.logger("validator.js","Validating encryption");
	
	// Temporary
	var encrypted = parent.encryption.encrypt(data);

	var decrypted;

	if (!isDecryptAble(encrypted)){
		parent.logger("validator.js","Decryption mismatched");
		return false;
	} else {
		decrypted = parent.encryption.decrypt(encrypted);
	}

	if (!isJson(decrypted)){
		parent.logger("validator.js","Either wrong encryption or not a JSON");
		return false;
	}
	parent.logger("validator.js","Data validated");
	return true;

}

function isJson(data){
	try {
        JSON.parse(data);
    } catch (e) {
    	parent.logger("validator.js","isJson error : "+e);
        return false;
    }
    return true;
}

function isDecryptAble(data){
	try{
		var decrypted = parent.encryption.decrypt(data);
	} catch (e) {
		parent.logger("validator.js","isDecryptAble error : "+e);
		return false;
	}
	return true;
}

export default validateData