import redis from 'redis';

var self;
var client;

export default class model{
    constructor(parent){
        self = parent;
        this.setKey = this.setKey.bind(this);
        this.updateIfEmpty = this.updateIfEmpty.bind(this);
        this.getKey = this.getKey.bind(this);
    }

    init(){
        client = redis.createClient({
            host: self.config.REDIS_HOST,
            port: self.config.REDIS_PORT,
            password: self.config.REDIS_PASSWORD
        });

        client.on('error', err => {
            self.logger("model.js","Redis error - "+err);
        });

        self.logger("model.js","Established connection with Redis");

        client.select(self.config.REDIS_DB_INDEX, function(err,res){
            self.logger("model.js","Redis select DB index "+self.config.REDIS_DB_INDEX);
        });
    }

    setKey(key,value){
        self.logger("model.js","Updating key : "+key+" , value : "+value);
        client.set(key, value, redis.print);
    }

    updateIfEmpty(key,value){
        self.model = this;
        this.getKey(key,function(err,res){
            if (!err){
                if (!res){
                    self.model.setKey(key,value);
                }
            }
        })
    }

    getKey(key,callback){
        client.get(key, function(err, response){
            if (!err){
                self.logger("model.js","Get key "+key+" = "+response);
                callback(null,response);
            } else {
                self.logger("model.js","Get key "+key+" error : "+err);
            }
        });
    }
}