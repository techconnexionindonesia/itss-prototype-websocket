import signal_state from '../config/initiation/signal_state.js';

var self;

export default class Signal{
	constructor(parent){
		self = parent;
		this.signal_state_prefix = "signal_state:";
	}

	updateSignalState(data,callback){
		self.logger("core.js","Updating signal state");
		if (typeof data.signal_name != 'undefined' && typeof data.signal_state != 'undefined'){
			var signal_name = data.signal_name;
			var signal_state = data.signal_state;
			self.model.setKey(this.signal_state_prefix+signal_name,signal_state);
			self.logger("core.js","state "+signal_name+" updated to "+signal_state);
			callback(null,JSON.stringify(data));
		}
	}

	initializeSignal(){
		self.logger("core.js","initialize signal");
		for(var i in signal_state){
			self.model.updateIfEmpty(i,signal_state[i]);
		}
	}
}