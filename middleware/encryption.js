import pkg from 'aes256';

var self;

export default class Encryption{
	constructor(parent){
		self = parent;
	}

	encrypt(data){
		var encrypted = "";
		try{
			encrypted = pkg.encrypt(self.config.AES256_ENCRYPTION, data);
		} catch(e) {
			self.logger("encryption.js","Encrypt error : "+e);
		}
		return encrypted;
	}

	decrypt(data){
		var decrypted = "";
		try{
			decrypted = pkg.decrypt(self.config.AES256_ENCRYPTION, data);
		} catch(e) {
			self.logger("encryption.js","Decrypt error : "+e);
		}
		return decrypted;
	}
}