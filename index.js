/**
 * Signal Updater websocket
 * ITSS-Prototype
 * 
 * Tech Connexion Indonesia
 */
import config from "./config/conf.js"
import logger from "./middleware/logger.js"
import {WebSocketServer} from 'ws';
import Core from "./middleware/core.js";
import model from './middleware/model.js';
import Encryption from './middleware/encryption.js';

/* Initialization */
var parent = {
   config: config,
   logger: logger,
}

var db = new model(parent);
db.init();

parent.model = db;
parent.encryption = new Encryption(parent);

const web_socket_server = new WebSocketServer({ port: config.PORT },()=>{
    logger("index.js","Websocket ITSS started");
});

web_socket_server.broadcast = function(data, sender) {
  web_socket_server.clients.forEach(function(client) {
    if (client !== sender) {
      client.send(data);
    }
  })
}

parent.web_socket_server = web_socket_server;

web_socket_server.on('connection', function connection(ws) {
   ws.on('message', (data) => {
      parent.ws = ws;
      var core = new Core(parent);
      core.processData(ws,data);
   })
});

web_socket_server.on('listening',()=>{
   logger("index.js","Listening on port on port : "+config.PORT);
   var core = new Core(parent);
   core.initializeEverything();
});